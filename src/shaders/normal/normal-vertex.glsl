#version 300 es

precision mediump float;

layout (location = 0) in vec3 aPosition;
layout (location = 1) in vec3 aNormal;

uniform mat4 mMatrix;
uniform mat4 pMatrix;
uniform mat4 vMatrix;
uniform float lineLength;

void main () {
  bool far = mod(float(gl_VertexID), 2.0) == 1.0;
  vec3 pos = far ? aPosition + aNormal * lineLength : aPosition;
  gl_Position = pMatrix * vMatrix * mMatrix * vec4(pos, 1.0);
}
