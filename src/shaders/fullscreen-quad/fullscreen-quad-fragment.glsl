#version 300 es

precision mediump float;

in vec2 vTextureCoordinates;

uniform sampler2D image;

out vec4 color;

void main() {
  color = texture(image, vTextureCoordinates);
}
