#version 300 es

precision highp float;

layout (location = 0) in vec3 aPosition;

uniform mat4 lightSpaceMatrix;
uniform mat4 mMatrix;

void main() {
  gl_Position = lightSpaceMatrix * mMatrix * vec4(aPosition, 1.0); 
}
