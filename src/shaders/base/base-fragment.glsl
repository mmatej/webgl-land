#version 300 es

precision highp float;

in vec4 vColor;
in vec3 vLightColor;
in vec2 vTextureCoordinates;
in vec4 vLightSpacePosition;
in vec3 vNormal;
in float elevation;

uniform sampler2D uvGridTexture;
uniform sampler2D depthTexture;

uniform vec3 lightDirection;

out vec4 color;

float shadow() {
  vec3 projectedCoordinates = vLightSpacePosition.xyz / vLightSpacePosition.w;
  projectedCoordinates = projectedCoordinates * 0.5 + 0.5;
  float shadowMapDepth = texture(depthTexture, vec2(projectedCoordinates.x, projectedCoordinates.y)).r;

  float bias = max(0.05 * (1.0 - dot(vNormal, lightDirection)), 0.01);
  return projectedCoordinates.z - bias <= shadowMapDepth ? 1.0 : 0.2;
}

void main() {
  vec4 textureColor = texture(uvGridTexture, vTextureCoordinates);
  if (elevation == 0.0) {
    // color = vec4(vec3(0.0, 0.42, 0.58), 1.0);
    color = vec4(vec3(0.0, 0.42, 0.58) * vLightColor * shadow(), 1.0);
  } else {
    // color = vec4(mix(vec3(0.38, 0.5, 0.2), vec3(0.5, 0.5, 0.5), elevation) * vLightColor * shadow(), 1.0);
    // color = vec4(mix(vec3(0.38, 0.5, 0.2), vec3(0.5, 0.5, 0.5), elevation) * shadow(), 1.0);
    color = vec4(textureColor.rgb * vLightColor * shadow(), textureColor.a); 
  }

  // color = vec4(vec3(1.0, 1.0, 1.0) * vLightColor, 1.0);
  // color = vec4(abs(vNormal), 1.0);
}
