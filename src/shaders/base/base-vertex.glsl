#version 300 es

precision highp float;

layout (location = 0) in vec3 aPosition;
layout (location = 1) in vec3 aNormal;
layout (location = 2) in vec4 aColor;
layout (location = 3) in vec2 aTextureCoordinates;

uniform mat4 mMatrix;
uniform mat4 pMatrix;
uniform mat4 vMatrix;
uniform mat4 lightSpaceMatrix;
uniform mat3 nMatrix;

uniform vec3 lightDirection;
uniform vec3 lightColor;

out vec4 vColor;
out vec3 vLightColor;
out vec2 vTextureCoordinates;
out vec4 vLightSpacePosition;
out vec3 vNormal;

out float elevation;

void main() {
  vColor = aColor;
  vTextureCoordinates = aTextureCoordinates;
  vNormal = aNormal;
  
  vec4 modelPosition = mMatrix * vec4(aPosition, 1.0);
  elevation = modelPosition.y;
  vLightSpacePosition = lightSpaceMatrix * modelPosition;

  vec3 transformedNormal = nMatrix * aNormal;
  float lightFactor = max(dot(transformedNormal, lightDirection), 0.0);
  
  vLightColor = lightColor * lightFactor;
  gl_Position = pMatrix * vMatrix * modelPosition;
}


