#version 300 es

precision mediump float;

in vec3 aPosition;

void main() {
  gl_Position = aPosition;
}

