import {
  create as createSquare,
  split,
  corners,
  middle,
  edgeMiddles
} from './square.js'

const index = columns => ([x, y]) => columns * y + x
const coordinates = columns => index => [index % columns, ~~(index / columns)]

const setCorners = (fn, map) => {
  const { heights, columns, rows } = map
  const cornerIndices = [
    [0, 0],
    [columns - 1, 0],
    [columns - 1, rows - 1],
    [0, rows - 1]
  ].map(c => index(columns)(c))

  const heightsCopy = heights.slice()
  cornerIndices.forEach(i => heightsCopy[i] = fn())

  return {
      ...map,
      heights: heightsCopy
  }
}

const randomzieCorners = map => setCorners(() => Math.random(), map)
const zeroCorners = map => setCorners(() => 0, map)

const init = (columns, rows) => ({
  heights: Array(rows * columns).fill(),
  columns,
  rows
})

const average = values => values.reduce((acc,v) => acc + v, 0) / values.length

const lift = fn => object => Object.keys(object).reduce((acc, key) => {
  acc[key] = fn(object[key])
  return acc
}, {})

export const generateHeightMap = (columns, rows, jitter, jitterDecay) => {
  const map = zeroCorners(init(columns, rows))
  const inx = index(columns)

  const recurse = (map, sq, jitter) => {
    if (sq.width < 3) {
      return
    }

    const liftedInx = lift(inx)
    const edgeMiddleIndices = liftedInx(edgeMiddles(sq))
    const cornerIndices = liftedInx(corners(sq))
    const squareMiddleIndex = inx(middle(sq))

    map.heights[edgeMiddleIndices.top] = map.heights[edgeMiddleIndices.top] !== undefined ? map.heights[edgeMiddleIndices.top] : average([map.heights[cornerIndices.topLeft], map.heights[cornerIndices.topRight]]) + Math.random() * jitter
    map.heights[edgeMiddleIndices.bottom] = map.heights[edgeMiddleIndices.bottom] !== undefined ? map.heights[edgeMiddleIndices.bottom] : average([map.heights[cornerIndices.bottomLeft], map.heights[cornerIndices.bottomRight]]) + Math.random() * jitter
    map.heights[edgeMiddleIndices.left] = map.heights[edgeMiddleIndices.left] !== undefined ? map.heights[edgeMiddleIndices.left] : average([map.heights[cornerIndices.topLeft], map.heights[cornerIndices.bottomLeft]]) + Math.random() * jitter
    map.heights[edgeMiddleIndices.right] = map.heights[edgeMiddleIndices.right] !== undefined ? map.heights[edgeMiddleIndices.right] : average([map.heights[cornerIndices.topRight], map.heights[cornerIndices.bottomRight]]) + Math.random() * jitter

    map.heights[squareMiddleIndex] =
      average([map.heights[edgeMiddleIndices.top],
      map.heights[edgeMiddleIndices.bottom],
      map.heights[edgeMiddleIndices.left],
      map.heights[edgeMiddleIndices.right]]) + Math.random() * jitter

    split(sq).forEach(s => recurse(map, s, jitter * jitterDecay * Math.sign(0.5 - Math.random())))
  }

  recurse(map, createSquare(0, 0, map.columns, map.rows), jitter)

  return map
}