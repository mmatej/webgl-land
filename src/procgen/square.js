export const create = (x, y, width, height) => ({ x, y, width, height })

export const split = ({ x, y, width, height }) => {
  const halfWidth = ~~(width / 2)
  const halfHeight = ~~(height / 2)
  const splitWidth = halfWidth + 1
  const splitHeight = halfHeight + 1

  const centerX = x + halfWidth
  const centerY = y + halfHeight

  return [
    create(x, y, splitWidth, splitHeight),
    create(centerX, y, splitWidth, splitHeight),
    create(x, centerY, splitWidth, splitHeight),
    create(centerX, centerY, splitWidth, splitHeight)
  ]
}

export const corners = ({ x, y, width, height }) => ({
  topLeft: [x, y],
  topRight: [x + width - 1, y],
  bottomLeft: [x, y + height - 1],
  bottomRight: [x + width - 1, y + height - 1]
})
  
export const edgeMiddles = ({ x, y, width, height }) => {
  const halfWidth = ~~(width / 2)
  const halfHeight = ~~(height / 2)

  return {
    top: [x + halfWidth, y],
    left: [x, y + halfHeight],
    bottom: [x + halfWidth, y + height - 1],
    right: [x + width - 1, y + halfHeight]
  }
}

export const middle = ({ x, y, width, height }) => [x + ~~(width / 2), y + ~~(height / 2)]