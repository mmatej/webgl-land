import { initWebGl2Context } from './webgl2-helpers/init.js'
import { fetchProgram } from './webgl2-helpers/shader.js'
import { mesh } from './webgl2-helpers/mesh.js'
import { plane, sphere } from './webgl2-helpers/geometry.js'

import { vec3, mat3, mat4 } from './gl-matrix/gl-matrix.js'

import {
  createNode,
  compute,
  traverse
} from './webgl2-helpers/scene-tree.js'

import { vertexNormals } from './webgl2-helpers/normals.js'
import { attributeBuffer } from './webgl2-helpers/buffer.js'
import { fetchTexture } from './webgl2-helpers/texture.js'

import { setupShadowMap, renderToShadowMap, lightSpaceMatrix } from './webgl2-helpers/shadows.js'
import { drawFullScreenTexture } from './webgl2-helpers/fullscreen-quad.js'

import { generateHeightMap } from './procgen/midpoint-displacement.js'
let debugShadowMap = false

window.addEventListener('keyup', () => {
  debugShadowMap = !debugShadowMap
})

const cWidth = 1920
const container = document.getElementById('container')
const gl = initWebGl2Context(container, {
  antialias: false,
  width: cWidth,
  height: ~~(cWidth * (container.clientHeight / container.clientWidth))
}, true)

const vMatrix = mat4.create()
mat4.translate(vMatrix, vMatrix, [0, -0.5, -2])
mat4.lookAt(vMatrix, [0, 10, 10], [0, 0, 0], [0, 1, 0])

const pMatrix = mat4.perspective(
  mat4.create(),
  Math.PI / 4,
  gl.drawingBufferWidth / gl.drawingBufferHeight,
  0.1,
  100
)

const nMatrix = mat3.create()

const scene = {}

setupApplication().then(v => {
  Object.assign(scene, v)
  draw()
})

function draw () {
  requestAnimationFrame(draw)
  gl.clearColor(0.0, 0.0, 0.0, 1.0)
  gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)
  scene.landNode.rotation.angle = performance.now() / 1000
  scene.planetNode.rotation.angle = -performance.now() / 2000
  compute(scene.rootNode, mat4.create())
  shadowPass(gl)
  gl.viewport(0, 0, gl.drawingBufferWidth, gl.drawingBufferHeight)
  drawScene(gl, scene.baseShaderProgram)
  if (debugShadowMap) {
    drawFullScreenTexture(gl, scene.shadowMap.texture)
  }
}

function shadowPass (gl) {
  gl.cullFace(gl.FRONT)
  renderToShadowMap(gl, scene.shadowMap, gl => {
    gl.useProgram(scene.shadowMapShaderProgram.program)
    traverse(scene.rootNode, node => {
      if (!node.payload) {
        return
      }
      
      const mesh = node.payload
      const { matrix } = node

      gl.uniformMatrix4fv(scene.shadowMapShaderProgram.uniforms.mMatrix, false, matrix)
      gl.uniformMatrix4fv(scene.shadowMapShaderProgram.uniforms.lightSpaceMatrix, false, scene.lsMatrix)

      gl.bindVertexArray(mesh.vao)
      gl.drawElements(gl.TRIANGLES, mesh.elementBuffer.numItems, gl.UNSIGNED_INT, 0)
      gl.bindVertexArray(null)
      gl.bindTexture(gl.TEXTURE_2D, null)

    })
  })
  gl.cullFace(gl.BACK)
}

const drawNode = (gl, program) => node => {

  if (!node.payload) {
    return
  }

  gl.useProgram(program.program)
  const mesh = node.payload
  const { matrix } = node

  mat3.fromMat4(nMatrix, matrix)
  mat3.invert(nMatrix, nMatrix)
  mat3.transpose(nMatrix, nMatrix)

  gl.uniformMatrix4fv(program.uniforms.mMatrix, false, matrix)
  gl.uniformMatrix4fv(program.uniforms.pMatrix, false, pMatrix)
  gl.uniformMatrix3fv(program.uniforms.nMatrix, false, nMatrix)
  gl.uniformMatrix4fv(program.uniforms.vMatrix, false, vMatrix)
  gl.uniformMatrix4fv(program.uniforms.lightSpaceMatrix, false, scene.lsMatrix)
  gl.uniform1i(program.uniforms.uvGridTexture, 0)
  gl.uniform1i(program.uniforms.depthTexture, 1)

  gl.uniform3fv(program.uniforms.lightDirection, scene.lightDirection)
  gl.uniform3fv(program.uniforms.lightColor, vec3.fromValues(1, 1, 1))

  gl.activeTexture(gl.TEXTURE0)
  gl.bindTexture(gl.TEXTURE_2D, scene.uvGridTexture)

  gl.activeTexture(gl.TEXTURE1)
  gl.bindTexture(gl.TEXTURE_2D, scene.shadowMap.texture)

  gl.bindVertexArray(mesh.vao)
  gl.drawElements(gl.TRIANGLES, mesh.elementBuffer.numItems, gl.UNSIGNED_INT, 0)
  gl.bindVertexArray(null)
  gl.bindTexture(gl.TEXTURE_2D, null);
}

function drawScene (gl, program) {
  traverse(scene.rootNode, drawNode(gl, program))
}

async function setupApplication () {
  const uvGridTexture = await fetchTexture(gl, 'textures/uv_grid.jpg')
  const mercuryTexture = await fetchTexture(gl, 'textures/2k_mercury.jpg')

  const baseShaderProgram = await fetchProgram(
    gl,
    '/src/shaders/base/base-vertex.glsl',
    '/src/shaders/base/base-fragment.glsl',
    [
      'aPosition',
      'aColor',
      'aNormal',
      'aTextureCoordinates'
    ],
    [
      'mMatrix',
      'pMatrix',
      'nMatrix',
      'vMatrix',
      'lightSpaceMatrix',
      'lightDirection',
      'lightColor',
      'uvGridTexture',
      'depthTexture'
    ])

  const normalShaderProgram = await fetchProgram(
    gl,
    'src/shaders/normal/normal-vertex.glsl',
    'src/shaders/normal/normal-fragment.glsl',
    ['aPosition', 'aNormal'],
    ['mMatrix', 'vMatrix', 'pMatrix', 'lineLength']
  )

  const shadowMapShaderProgram = await fetchProgram(
    gl,
    'src/shaders/shadowmap/shadowmap-vertex.glsl',
    'src/shaders/shadowmap/shadowmap-fragment.glsl',
    ['aPosition'],
    ['lightSpaceMatrix', 'mMatrix']
  )

  const shadowMap = setupShadowMap(gl, 1024, 1024)

  const lightDirection = vec3.fromValues(-1, -0.5, 0)
  vec3.normalize(lightDirection, lightDirection)

  vec3.scale(lightDirection, lightDirection, -1)
  const lsMatrix = lightSpaceMatrix(lightDirection, 15, -10, 10)

  const hm = generateHeightMap(257, 257, 0.2, 0.5)
  const maxHeight = Math.max(1.0, hm.heights.reduce((acc, value) => Math.max(acc, value)))

  hm.heights = hm.heights.map(value => value / maxHeight)
  const side = 10
  const p = plane(side, side, hm.columns, hm.rows)
  p.vertices = p.vertices.map(([x, y, z], i) => {
    const nx = x / side
    const nz = z / side
    const factor = Math.sin(nz * Math.PI) * Math.sin(nx * Math.PI)
    return [x, side * factor * Math.max(0, hm.heights[i]), z]
  })
  


  const mNormals = vertexNormals(p)
  const m = mesh(gl, baseShaderProgram,
    {
      aPosition: p.vertices,
      aColor: p.vertices.map(() => [Math.random(), Math.random(), Math.random(), 1]),
      aNormal: mNormals,
      aTextureCoordinates: p.uvs
    },
    p.indices
  )

  const sphereGeometry = sphere(1,32, 32)
  console.log(sphereGeometry)
  const sphereNormals = vertexNormals(sphereGeometry)
  const planet = mesh(gl, baseShaderProgram,
    {
      aPosition: sphereGeometry.vertices,
      aColor: sphereGeometry.vertices.map(() => [1, 1, 1, 1]),
      aNormal: sphereNormals,
      aTextureCoordinates: sphereGeometry.uvs
    },
    sphereGeometry.indices
  )

  const rootNode = createNode()
  const landNode = createNode(m, [-side / 2, 0, -side / 2], { angle: 0.0, axes: [0, 1, 0] })
  rootNode.children.push(landNode)
  const planetNode = createNode(planet, [5, 3, 0], {angle: 0, axes: [1, 1, 0]})
  rootNode.children.push(planetNode)

  gl.enable(gl.DEPTH_TEST)

  return {
    baseShaderProgram,
    normalShaderProgram,
    shadowMapShaderProgram,
    m,
    mNormals,
    uvGridTexture,
    mercuryTexture,
    lightDirection,
    shadowMap,
    lsMatrix,
    planet,
    rootNode,
    landNode,
    planetNode
  }

  function normalLinesGeometry (vertices) {
    return vertices.reduce((acc, v) => acc.concat([v, v]), [])
  }
}
