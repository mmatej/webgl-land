const defaultOptions = {
  antialias: true
}

export function initWebGl2Context (container, options, useScreenDpr = true) {
  if (!container) {
    console.error('initWebGl2Context needs to be called with a DOMElement argument \'container\'')
  }
  const dpr = useScreenDpr ? window.devicePixelRatio : 1

  const canvas = createCanvas(
    options.width || container.clientWidth,
    options.height || container.clientHeight,
    dpr)
  console.log(options)
  container.appendChild(canvas)

  const contextOptions = Object.assign({}, defaultOptions, options)

  const gl = canvas.getContext('webgl2', contextOptions)
  if (!gl) {
    console.error('Error creating WebGL2 context')
  }

  return gl
}

function createCanvas (width, height, devicePixelRatio) {
  const canvas = document.createElement('canvas')
  canvas.width = width * devicePixelRatio
  canvas.height = height * devicePixelRatio

  return canvas
}
