export function plane (width, height, widthSegments, heightSegments) {
  const widthSegmentSize = width / widthSegments
  const heightSegmentSize = height / heightSegments

  const vertices = []
  const uvs = []
  for (let z = 0; z < heightSegments; ++z) {
    const zCoord = z * heightSegmentSize
    for (let x = 0; x < widthSegments; ++x) {
      const xCoord = x * widthSegmentSize
      vertices.push([xCoord, 0, zCoord])
      uvs.push([x / widthSegments, z / heightSegments])
    }
  }

  const indices = []
  const getIndexAt = coordinateIndexFactory(widthSegments)
  for (let z = 0; z < heightSegments - 1; ++z) {
    for (let x = 0; x < widthSegments - 1; ++x) {
      const topLeft = getIndexAt(x, z + 1)
      const bottomLeft = getIndexAt(x, z)
      const topRight = getIndexAt(x + 1, z + 1)
      const bottomRight = getIndexAt(x + 1, z)

      indices.push(
        topLeft,
        bottomLeft,
        bottomRight,
        topLeft,
        bottomRight,
        topRight
      )
    }
  }

  return {
    indices,
    vertices,
    uvs
  }
}

export function coordinateExtractorFactory (arr, width) {
  const index = coordinateIndexFactory(width)
  return function coordinateExtractor (x, y) {
    return arr[index(x, y)]
  }
}

function coordinateIndexFactory (width) {
  return function coordinateIndex (x, y) {
    return y * width + x
  }
}

export function sphere (radius, meridians, parallels) {
  const vertices = []
  const uvs = []
  for (let i = 1; i < parallels - 1; ++i) {
    const polar = Math.PI * i / (parallels - 1)
    for (let j = 0; j < meridians; ++j) {
      const azimuth = 2.0 * Math.PI * j / (meridians - 1)
      vertices.push(sphericalToCartesian(radius, azimuth, polar))
      uvs.push([1 - j / (meridians - 1), i / (parallels - 1)])
    }
  }

  const indices = []

  for (let i = 0; i < parallels - 3; ++i) {
    const currentRowStart = meridians * i
    const nextRowStart = (meridians * (i + 1))

    for (let j = 0; j < meridians; ++j) {
      indices.push(
        currentRowStart + j,
        nextRowStart + j,
        nextRowStart + (j + 1) % meridians,

        currentRowStart + j,
        nextRowStart + (j + 1) % meridians,
        currentRowStart + (j + 1) % meridians
      )
    }
  }

  return {
    vertices,
    indices,
    uvs
  }
}

function sphericalToCartesian (radius, azimuth, polar) {
  return [
    radius * Math.sin(polar) * Math.cos(azimuth),
    radius * Math.cos(polar),
    radius * Math.sin(polar) * Math.sin(azimuth)
  ]
}
