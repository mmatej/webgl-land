import { mat4 } from '../gl-matrix/gl-matrix.js'

export function setupShadowMap (gl, width, height) {
  const fbo = gl.createFramebuffer()
  const texture = gl.createTexture()
  gl.bindTexture(gl.TEXTURE_2D, texture)
  gl.texImage2D(gl.TEXTURE_2D, 0, gl.DEPTH_COMPONENT24, width, height, 0, gl.DEPTH_COMPONENT, gl.UNSIGNED_INT, null)
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.REPEAT)
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.REPEAT)
  gl.texParameterf(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST)
  gl.texParameterf(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST)
  gl.bindTexture(gl.TEXTURE_2D, null)

  gl.bindFramebuffer(gl.DRAW_FRAMEBUFFER, fbo)
  gl.framebufferTexture2D(gl.DRAW_FRAMEBUFFER, gl.DEPTH_ATTACHMENT, gl.TEXTURE_2D, texture, 0)
  gl.drawBuffers([gl.NONE])
  gl.readBuffer(gl.NONE)
  gl.bindFramebuffer(gl.DRAW_FRAMEBUFFER, null)

  return {
    width,
    height,
    fbo,
    texture
  }
}

export function renderToShadowMap (gl, shadowMap, drawFunction) {
  gl.viewport(0, 0, shadowMap.width, shadowMap.height)
  gl.bindFramebuffer(gl.DRAW_FRAMEBUFFER, shadowMap.fbo)
  gl.clear(gl.DEPTH_BUFFER_BIT)
  drawFunction(gl)
  gl.bindFramebuffer(gl.DRAW_FRAMEBUFFER, null)
}

export function lightSpaceMatrix (lightDirection, projectionRectSide, near, far) {
  const halfSide = projectionRectSide / 2
  const projection = mat4.ortho(
    mat4.create(),
    -halfSide,
    halfSide,
    -halfSide,
    halfSide,
    near,
    far
  )
  const view = mat4.lookAt(
    mat4.create(),
    lightDirection,
    [0, 0, 0],
    [0, 1, 0]
  )

  return mat4.multiply(mat4.create(), projection, view)
}
