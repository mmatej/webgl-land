import { mat4 } from '../gl-matrix/gl-matrix.js'

export const compute = (node, matrix) => {
    mat4.copy(node.matrix, matrix)
    mat4.rotate(node.matrix, node.matrix, node.rotation.angle, node.rotation.axes)
    mat4.translate(node.matrix, node.matrix, node.position)
    node.children.forEach(n => compute(n, node.matrix))
}

export const traverse = (node, callback) => {
    callback(node)
    node.children.forEach(n => traverse(n, callback))
}

export const createNode = (payload, position = [0, 0, 0], rotation = {angle: 0, axes: [0, 0, 0]}, children = []) => ({
    payload,
    position,
    rotation,
    children,
    matrix: mat4.create()
})