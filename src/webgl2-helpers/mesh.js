import {attributeBuffer, elementBuffer} from './buffer.js'

export function mesh (gl, program, attributeData, elements) {
  const vao = gl.createVertexArray()
  gl.bindVertexArray(vao)

  const attributeBuffers = {}

  const attributeNames = Object.keys(program.attributes)

  attributeNames.forEach(name => {
    if (!(name in attributeData)) {
      console.error(`Property '${name}' missing in attributeData.`)
    }
    const bufferObject = attributeBuffers[name] = attributeBuffer(gl, attributeData[name])
    gl.enableVertexAttribArray(program.attributes[name])
    gl.bindBuffer(gl.ARRAY_BUFFER, bufferObject.buffer)
    gl.vertexAttribPointer(program.attributes[name], bufferObject.itemSize, gl.FLOAT, false, 0, 0)
  })

  const eb = elementBuffer(gl, elements)
  gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, eb.buffer)
  gl.bindVertexArray(null)

  return {
    attributeBuffers,
    elementBuffer: eb,
    vao
  }
}
