export function attributeBuffer (gl, data) {
  if (!Array.isArray(data)) {
    console.error('data parameter needs to be an array when creating an attribtue buffer')
  } else if (data.length === 0) {
    console.error('empty data array when creating an attribute buffer')
  }

  const buffer = gl.createBuffer()
  gl.bindBuffer(gl.ARRAY_BUFFER, buffer)
  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(flattenArray(data)), gl.STATIC_DRAW)

  const itemSize = calculateItemSize(data[0])
  const numItems = data.length

  return {buffer, itemSize, numItems}
}

export function elementBuffer (gl, data) {
  if (!Array.isArray(data)) {
    console.error('data parameter needs to be an array when creating an attribtue buffer')
  } else if (data.length === 0) {
    console.error('empty data array when creating an attribute buffer')
  }

  const buffer = gl.createBuffer()
  gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, buffer)
  gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint32Array(data), gl.STATIC_DRAW)

  const itemSize = 1
  const numItems = data.length

  return {buffer, itemSize, numItems}
}

export function flattenArray (arr) {
  return Array.prototype.concat.apply([], arr)
}

function calculateItemSize (item) {
  if (!Array.isArray(item)) {
    return 1
  }

  return item.length
}
