export function pushMatrix (stack) {
  const current = stack.matrix
  if (++stack.activeIndex >= stack.matrices.length) {
    const gap = stack.activeIndex - stack.matrices.length
    for (let i = 0; i <= gap; ++i) {
      stack.matrices.push(stack.cloneFunction(current))
    }
  }

  stack.updateFunction(stack.matrix, current)
}

export function popMatrix (stack) {
  if (--stack.activeIndex < 0) {
    console.error('Matrix stack empty.')
    stack.activeIndex = 0
  }
}

export function create (matrixFactory, cloneFunction, updateFunction) {
  return {
    activeIndex: 0,
    matrices: [
      matrixFactory()
    ],
    matrixFactory,
    cloneFunction,
    updateFunction,
    get matrix () {
      return this.matrices[this.activeIndex]
    }
  }
}
