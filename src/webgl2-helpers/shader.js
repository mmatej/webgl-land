export function compileShader (gl, source, type) {
  const shader = gl.createShader(type)
  gl.shaderSource(shader, source)
  gl.compileShader(shader)

  if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
    console.error(gl.getShaderInfoLog(shader))
  }

  return shader
}

export function createProgram (gl, vertexShaderSource, fragmentShaderSource, attributeNames, uniformNames) {
  const program = gl.createProgram()
  gl.attachShader(program, compileShader(gl, vertexShaderSource, gl.VERTEX_SHADER))
  gl.attachShader(program, compileShader(gl, fragmentShaderSource, gl.FRAGMENT_SHADER))

  gl.linkProgram(program)

  if (!gl.getProgramParameter(program, gl.LINK_STATUS)) {
    console.error('Error linking shader program.')
  }

  const attributes = attributeNames.reduce((acc, name) =>
    Object.assign(acc, {[name]: gl.getAttribLocation(program, name)}), {})
  const uniforms = uniformNames.reduce((acc, name) =>
    Object.assign(acc, {[name]: gl.getUniformLocation(program, name)}), {})

  return {program, attributes, uniforms}
}

export async function fetchProgram (
  gl,
  vertexShaderSourceUrl,
  fragmentShaderSourceUrl,
  attributeNames,
  uniformNames) {
  return Promise.all([fetch(vertexShaderSourceUrl), fetch(fragmentShaderSourceUrl)])
    .then(responses =>
      Promise.all(responses.map(r => r.text()))).then(sources =>
      createProgram(gl, sources[0], sources[1], attributeNames, uniformNames))
}
