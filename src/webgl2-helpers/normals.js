export const faceNormals = geometry => {
  const normals = []
  for (let i = 0; i < geometry.indices.length; i += 3) {
    const a = geometry.vertices[geometry.indices[i]]
    const b = geometry.vertices[geometry.indices[i + 1]]
    const c = geometry.vertices[geometry.indices[i + 2]]

    const asb = subv(a, b)
    const asc = subv(a, c)

    const n = normalize(cross(asc, asb))
    if (Number.isNaN(n[0])) {
      console.log('--------------------------------')
      console.log(a, b, c, asb, asc, cross(asc, asb))
    }
    normals.push(n, n, n)
  }

  return normals
}

export const vertexNormals = geometry => {
  const fn = faceNormals(geometry)
  const normals = new Array(geometry.vertices.length).fill().map(() => [0, 0, 0])

  geometry.indices.forEach((v, i) => {
    const vertexNormal = normals[v]
    const faceNormal = fn[i]
    normals[v] = addv(vertexNormal, faceNormal)
  })

  return normals.map(normalize)
}

function subv (a, b) {
  return a.map((v, i) => a[i] - b[i])
}

function addv (a, b) {
  return a.map((v, i) => a[i] + b[i])
}

function cross (a, b) {
  return [
    a[1] * b[2] - a[2] * b[1],
    a[2] * b[0] - a[0] * b[2],
    a[0] * b[1] - a[1] * b[0]
  ]
}

function normalize (v) {
  const l = lenv(v)
  return [
    v[0] / l,
    v[1] / l,
    v[2] / l
  ]
}

function lenv (v) {
  return Math.sqrt(
    Math.pow(v[0], 2) +
    Math.pow(v[1], 2) +
    Math.pow(v[2], 2)
  )
}
