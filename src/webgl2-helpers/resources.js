export async function loadShaderSources (vertexShader, fragmentShader) {
  return Promise.all([
    fetch(vertexShader),
    fetch(fragmentShader)]).then(responses => Promise.all([
    responses[0].text(),
    responses[1].text()]))
}
