import { mesh } from './mesh.js'
import { createProgram } from './shader.js'

const vertexShader = `#version 300 es
  precision mediump float;

  layout (location = 0) in vec2 aPosition;
  layout (location = 1) in vec2 aTextureCoordinates;

  out vec2 vTextureCoordinates;

  void main () {
    vTextureCoordinates = aTextureCoordinates;
    gl_Position = vec4(aPosition, 0.0, 1.0);
  }`

const fragmentShader = `#version 300 es
  precision mediump float;

  in vec2 vTextureCoordinates;

  uniform sampler2D image;

  out vec4 color;

  void main() {
      color = vec4(texture(image, vTextureCoordinates).xyz, 1.0);
  }`

function fullScreenQuadMesh (gl, program) {
  const vertices = [
    [-1, 1],
    [-1, -1],
    [1, -1],
    [1, 1]
  ]

  const uvs = [
    [0, 1],
    [0, 0],
    [1, 0],
    [1, 1]
  ]

  const indices = [0, 1, 2, 0, 2, 3]

  const quadMesh = mesh(gl, program, {
    'aPosition': vertices,
    'aTextureCoordinates': uvs
  }, indices)

  return quadMesh
}

export function drawFullScreenTexture (gl, texture) {
  const program = createProgram(
    gl,
    vertexShader,
    fragmentShader,
    ['aPosition', 'aTextureCoordinates'],
    ['image']
  )
  const quadMesh = fullScreenQuadMesh(gl, program)

  gl.useProgram(program.program)
  gl.uniform1i(program.uniforms.image, 0)

  gl.activeTexture(gl.TEXTURE0)

  gl.bindTexture(gl.TEXTURE_2D, texture)
  gl.bindVertexArray(quadMesh.vao)
  gl.drawElements(gl.TRIANGLES, quadMesh.elementBuffer.numItems, gl.UNSIGNED_INT, 0)
  gl.bindVertexArray(null)
  gl.bindTexture(gl.TEXTURE_2D, null)
}
